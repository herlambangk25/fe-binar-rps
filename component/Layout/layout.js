import { Content } from "./Content.js";
import * as BoxJs from "./Box.js";

export const Layout = ({ children }) => (
  <BoxJs.Box
    css={{
      maxW: "100%",
    }}
  >
    {children}
    <Content />
  </BoxJs.Box>
);
