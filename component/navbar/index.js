import React from "react";
import {
  Navbar,
  Button,
  Link,
  Text,
  Dropdown,
  Input,
  Row,
  Checkbox,
  Modal,
} from "@nextui-org/react";
// import { Layout } from "../Layout/layout";
import { Logo } from "../logo/logo";
// import { VariantsSelectorWrapper } from "./VariantsSelectorWrapper.js";
import { useRouter } from "next/router";
import { icons } from "./icons";

import LoginPage from "../../pages/loginPage";
import RegisterPage from "../../pages/registerPage";
import NotLogin from "../../component/navbar/notLogin";
import { useSelector } from "react-redux";
import NavLogin from "./navLogin";

function HomeNavbar() {
  const { username, email } = useSelector((state) => state.auth);

  const router = useRouter();
  const currentRoute = router.pathname;
  console.log(...currentRoute);

  const [visible, setVisible] = React.useState(false);
  const handleShow = () => setVisible(true);
  // const registerHandler = () => setVisible(true);

  const closeHandler = () => {
    setVisible(false);
    console.log("closed");
  };

  console.log("username,", username);
  console.log("email,", email);

  return (
    <Navbar isBordered variant="sticky">
      <Navbar.Brand>
        <Logo />
        <Text b color="inherit" hideIn="xs">
          ACME
        </Text>
      </Navbar.Brand>
      {/* <Navbar.Content activeColor={activeColor} hideIn="xs" variant={variant}> */}
      <Navbar.Content
        enableCursorHighlight
        activeColor="primary"
        hideIn="xs"
        variant="highlight"
      >
        <Navbar.Link href="/" isActive={currentRoute === "/"}>
          Home
        </Navbar.Link>
        <Navbar.Link
          href="/player-list"
          isActive={currentRoute === "/player-list"}
        >
          Player List
        </Navbar.Link>

        <Navbar.Link href="/allGame">All Game</Navbar.Link>

        <Dropdown isBordered>
          <Navbar.Item>
            <Dropdown.Button
              auto
              light
              css={{
                px: 0,
                dflex: "center",
                svg: { pe: "none" },
              }}
              iconRight={icons.chevron}
              ripple={false}
            >
              Features
            </Dropdown.Button>
          </Navbar.Item>
          <Dropdown.Menu
            aria-label="ACME features"
            css={{
              $$dropdownMenuWidth: "340px",
              $$dropdownItemHeight: "70px",
              "& .nextui-dropdown-item": {
                py: "$4",
                // dropdown item left icon
                svg: {
                  color: "$secondary",
                  mr: "$4",
                },
                // dropdown item title
                "& .nextui-dropdown-item-content": {
                  w: "100%",
                  fontWeight: "$semibold",
                },
              },
            }}
          >
            <Dropdown.Item
              key="autoscaling"
              showFullDescription
              description="ACME scales apps to meet user demand, automagically, based on load."
              icon={icons.scale}
            >
              Autoscaling
            </Dropdown.Item>
            <Dropdown.Item
              key="usage_metrics"
              showFullDescription
              description="Real-time metrics to debug issues. Slow query added? We’ll show you exactly where."
              icon={icons.activity}
            >
              Usage Metrics
            </Dropdown.Item>
            <Dropdown.Item
              key="production_ready"
              showFullDescription
              description="ACME runs on ACME, join us and others serving requests at web scale."
              icon={icons.flash}
            >
              Production Ready
            </Dropdown.Item>
            <Dropdown.Item
              key="99_uptime"
              showFullDescription
              description="Applications stay on the grid with high availability and high uptime guarantees."
              icon={icons.server}
            >
              +99% Uptime
            </Dropdown.Item>
            <Dropdown.Item
              key="supreme_support"
              showFullDescription
              description="Overcome any challenge with a supporting team ready to respond."
              icon={icons.user}
            >
              +Supreme Support
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Navbar.Content>
      <Navbar.Content>
        {username == "" ? <NotLogin /> : <NavLogin />}
      </Navbar.Content>
    </Navbar>

    // <Layout>

    //   {/* <VariantsSelectorWrapper>
    //     <Card css={{ px: "$6", maxW: "90%" }}>
    //       <Card.Body>
    //         <Radio.Group
    //           defaultValue="default"
    //           label="Select active variant"
    //           orientation="horizontal"
    //           size="sm"
    //           value={variant}
    //           onChange={setVariant}
    //         >
    //           {variants.map((variant) => (
    //             <Radio key={variant} color={activeColor} value={variant}>
    //               {variant}
    //             </Radio>
    //           ))}
    //         </Radio.Group>
    //         <Spacer y={0.5} />
    //         <Radio.Group
    //           defaultValue="default"
    //           label="Select active color"
    //           orientation="horizontal"
    //           size="sm"
    //           value={activeColor}
    //           onChange={setActiveColor}
    //         >
    //           {colors.map((color) => (
    //             <Radio key={color} color={activeColor} value={color}>
    //               {color === "primary" ? "primary (default)" : color}
    //             </Radio>
    //           ))}
    //         </Radio.Group>
    //       </Card.Body>
    //     </Card>
    //   </VariantsSelectorWrapper> */}
    // </Layout>
  );
}
export default HomeNavbar;
