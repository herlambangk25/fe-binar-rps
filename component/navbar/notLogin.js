import React, { useState } from "react";
import {
  Navbar,
  Button,
  Link,
  Text,
  Dropdown,
  Input,
  Row,
  Checkbox,
  Modal,
} from "@nextui-org/react";
import { Logo } from "../logo/logo";
import { useRouter } from "next/router";
import { icons } from "./icons";

// import notLogin from "../../pages/loginPage";
import LoginPage from "../../pages/loginPage";
import { useSelector } from "react-redux";
import RegisterPage from "../../pages/registerPage";

function NotLogin() {
  const { username } = useSelector((state) => state.auth);

  const router = useRouter();
  const currentRoute = router.pathname;
  console.log(...currentRoute);

  const [visible, setVisible] = useState(false);
  const handleShow = () => setVisible(true);

  const closeHandler = () => {
    setVisible(false);
    console.log("closed");
  };

  console.log(username);

  return (
    <>
      <Navbar.Item>
        <>
          <Button auto color="warning" shadow onPress={handleShow}>
            Login
          </Button>
          <Modal
            closeButton
            blur
            aria-labelledby="modal-title-login"
            open={visible}
            onClose={closeHandler}
            data-bs-target="#login"
          >
            <LoginPage id="login" />
          </Modal>
        </>
      </Navbar.Item>

      <Navbar.Item>
        <>
          <Button auto color="primary" shadow onPress={handleShow}>
            Register
          </Button>
          <Modal
            closeButton
            blur
            aria-labelledby="modal-title-register"
            open={visible}
            onClose={closeHandler}
            data-bs-target="#register"
          >
            <LoginPage id="login" />
            {/* <RegisterPage id="register" /> */}
          </Modal>
        </>
      </Navbar.Item>
    </>
  );
}
export default NotLogin;
