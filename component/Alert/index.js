import React from "react";
import { Alert } from "react-bootstrap";

function SAlert({ message, type }) {
  return <Alert variant={type}>{message}</Alert>;
}

export default SAlert;


       <Navbar.Item>
          <>
            <Button auto color="warning" shadow onPress={handleShow}>
              Login
            </Button>
            <Modal
              closeButton
              blur
              aria-labelledby="modal-title-login"
              open={visible}
              onClose={closeHandler}
              data-bs-target="#login"
            >
              {/* <LoginPage id="login" /> */}
            </Modal>
          </>
        </Navbar.Item>

        <Navbar.Item>
          <>
            <Button auto color="primary" shadow onPress={handleShow}>
              Register
            </Button>
            <Modal
              closeButton
              blur
              aria-labelledby="modal-title-register"
              open={visible}
              onClose={closeHandler}
              data-bs-target="#register"
            >
              <LoginPage id="login" />
              {/* <RegisterPage id="register" /> */}
            </Modal>
          </>
        </Navbar.Item>