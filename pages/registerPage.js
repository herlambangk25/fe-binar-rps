import { Button, Checkbox, Input, Modal, Row, Text } from "@nextui-org/react";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { icons } from "../component/navbar/icons";

export default function RegisterPage({ id }) {
  const [visible, setVisible] = useState(false);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [city, setCity] = useState("");
  const router = useRouter();

  const closeHandler = () => {
    setVisible(false);
    console.log("closed");
  };
  const handleSubmit = async (event) => {
    // event.preventDefault();
    try {
      const result = await axios.post(
        "https://binar-1add3-default-rtdb.firebaseio.com/",
        {
          email: email,
          username: username,
          password: password,
          city: city,
        }
      );
      alert(result.data.message);
      router.push("/");
    } catch (err) {
      alert("Your email or username is already in use.");
      window.location.reload();
    }
  };

  return (
    <>
      <Modal.Header>
        <Text id="modal-title-login" size={18}>
          Sign Up to{" "}
          <Text b size={18}>
            GamerWeb
          </Text>
        </Text>
      </Modal.Header>
      <Modal.Body>
        <Input
          clearable
          bordered
          fullWidth
          color="primary"
          size="lg"
          id="username"
          onChange={(event) => {
            setUsername(event.target.value);
          }}
          placeholder="Username"
          // contentLeft={<Mail fill="currentColor" />}
          contentLeft={icons.mail}
        />
        <Input
          clearable
          bordered
          fullWidth
          color="primary"
          size="lg"
          id="email"
          onChange={(event) => {
            setEmail(event.target.value);
          }}
          placeholder="Email"
          // contentLeft={<Mail fill="currentColor" />}
          contentLeft={icons.mail}
        />
        <Input
          clearable
          bordered
          fullWidth
          color="primary"
          size="lg"
          id="password"
          onChange={(event) => {
            setPassword(event.target.value);
          }}
          placeholder="Password"
          contentLeft={icons.password}
          // contentLeft={<Password fill="currentColor" />}
        />
        <Input
          clearable
          bordered
          fullWidth
          color="primary"
          size="lg"
          id="city"
          onChange={(event) => {
            setCity(event.target.value);
          }}
          placeholder="Your City"
          contentLeft={icons.password}
          // contentLeft={<Password fill="currentColor" />}
        />
        <Row justify="space-between">
          <Checkbox>
            <Text size={14}>Remember me</Text>
          </Checkbox>
          <Text size={14}>Forgot password?</Text>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button auto flat color="error" onPress={closeHandler}>
          Close
        </Button>
        <Button auto onClick={handleSubmit}>
          Sign up
        </Button>
      </Modal.Footer>
    </>
  );
}
