import { NextUIProvider } from "@nextui-org/react";
import { StrictMode } from "react";
import { Provider } from "react-redux";
import "../styles/globals.css";
import { store } from "../app/store";
function MyApp({ Component, pageProps }) {
  return (
    <StrictMode>
      <Provider store={store}>
        <NextUIProvider>
          <Component {...pageProps} />
        </NextUIProvider>
      </Provider>
    </StrictMode>
  );
}
export default MyApp;
