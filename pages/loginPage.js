import { Button, Checkbox, Input, Modal, Row, Text } from "@nextui-org/react";
import axios, { formToJSON } from "axios";
import React, { useState } from "react";
import { icons } from "../component/navbar/icons";
// import SAlert from "../component/Alert";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { auth } from "../features/authSlice";

export default function LoginPage() {
  const [visible, setVisible] = React.useState(false);
  const router = useRouter();
  const dispatch = useDispatch();
  const handler = () => setVisible(true);
  const closeHandler = () => {
    setVisible(false);
    console.log("closed");
  };

  // const [form, setForm] = useState({
  //   username: "",
  //   password: "",
  // });
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  // const handleChange = async (e) => {
  //   setForm({ ...form, [e.target.name]: [e.target.value] });
  // };

  const handleSumbit = async (event) => {
    // event.preventDefault();

    try {
      const res = await axios.post("http://localhost:4000/login", {
        username: username,
        password: password,
      });
      // console.log(form.username);
      router.push("/");
      dispatch(auth({ username }));
      alert(res.data.message);
      window.location.reload();

      console.log(res);
    } catch (err) {
      window.alert(err);
      console.log(err.message);
    }
  };

  return (
    <>
      {/* <div className="m-auto" style={{ width: "50%" }}>
        {alert.status && <SAlert type={alert.type} message={alert.message} />}
      </div> */}
      <Modal.Header>
        <Text id="modal-title-register" size={18}>
          Welcome to{" "}
          <Text b size={18}>
            GamerWeb
          </Text>
        </Text>
      </Modal.Header>
      <Modal.Body>
        <Input
          clearable
          bordered
          fullWidth
          color="primary"
          size="lg"
          name="username"
          // onChange={handleChange}
          onChange={(event) => {
            setUsername(event.target.value);
          }}
          placeholder="Username"
          // contentLeft={<Mail fill="currentColor" />}
          contentLeft={icons.mail}
        />
        <Input
          clearable
          bordered
          fullWidth
          color="primary"
          size="lg"
          name="password"
          // onChange={handleChange}
          onChange={(event) => {
            setPassword(event.target.value);
          }}
          placeholder="Password"
          contentLeft={icons.password}
          // contentLeft={<Password fill="currentColor" />}
        />
        <Row justify="space-between">
          <Checkbox>
            <Text size={14}>Remember me</Text>
          </Checkbox>
          <Text size={14}>Forgot password?</Text>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button auto flat color="error" onPress={closeHandler}>
          Close
        </Button>
        <Button auto onPress={handleSumbit}>
          Login in
        </Button>
      </Modal.Footer>
    </>
  );
}
